**Dokuz Eylül Üniversitesi Fen Bilimleri Enstitüsünün** uygulamakta olduğu tez yazım kurallarına uygun bir biçimde LaTeX kullanılarak hazırlanmış örnek tez şablonudur.

Tamamen kişisel kullanım amacıyla hazırlanmıştır, Enstitü ile bir bağlantısı yoktur. İhtiyacı olan herkes kullanabilir.

Yüksek lisans ve doktora için uygundur, Türkçe lisansütü eğitim veren bölümler için Türkçesi de yapılmıştır. Bize gelen bilgiler ışığında güncellemeler yapılmaktadır.

TeX dosyasının içinde nasıl kullanılacağı ile ilgili notlar bulunmaktadır.

Hazırlayanlar:

**Cem Çelik, Volkan Öğer**

DEU Fen Fakültesi Matematik Bölümü B251/3 Nolu Oda.